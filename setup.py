from distutils.core import setup

setup(
    name='Grand Vizier',
    version='0.1',
    packages=['vizier'],
    long_description=open('README.md').read(),
    include_package_data=True
)