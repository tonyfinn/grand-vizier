def calculate_calendar_date(elapsed_time: float) -> str:
    elapsed_days = elapsed_time / 5
    week = int(elapsed_days // 7)
    day = int(elapsed_days % 7)
    return 'W{} D{}'.format(week + 1, day + 1)