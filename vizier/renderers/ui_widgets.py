from math import floor
from typing import Dict, Tuple, TYPE_CHECKING

from kivy.properties import BooleanProperty, NumericProperty, ObjectProperty, StringProperty
from kivy.graphics.context_instructions import Color
from kivy.graphics.vertex_instructions import Rectangle

from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.label import Label
from kivy.uix.relativelayout import RelativeLayout

if TYPE_CHECKING:
    from .map_ui import PopupPanel, HasPopupPanel

class PopupPanelLayer(FloatLayout):

    active_popups = {} # type: Dict[int, HasPopupPanel]

    def add_popup_panel(self, target: 'HasPopupPanel') -> None:
        psid = target.popup_source_id
        if psid in PopupPanelLayer.active_popups:
            return
        panel = target.build_popup_panel()
        remover = self.panel_remover(psid, target)
        PopupPanelLayer.active_popups[psid] = remover
        self.add_widget(panel)
        panel.bind(on_close=remover)

    def panel_remover(self, psid, target):
        def remove_panel(panel):
            panel.unbind(on_close=PopupPanelLayer.active_popups[psid])
            del self.active_popups[psid]
            self.remove_widget(panel)
            target.popup_closed()
        return remove_panel
        

class VzProgressBar(RelativeLayout):
    max_value = NumericProperty(100)
    min_value = NumericProperty(0)
    display_text = BooleanProperty(True)
    text_color = ObjectProperty((0, 0, 0, 1))
    pb_value = NumericProperty(50)
    bg_color = ObjectProperty((1, 0, 0))
    fg_color = ObjectProperty((0, 1, 0))
    text_content = StringProperty('{}')
    
    def __init__(self, *args, **kwargs):
        self.initialized = False
        super().__init__(*args, **kwargs)
        with self.canvas.before:
            Color(*self.bg_color)
            self.outer_rect = Rectangle(size=(100, 20), pos=(5, 5))
            Color(*self.fg_color)
            self.inner_rect = Rectangle(size=(94, 16), size_hint=(None, None), pos=(7, 7))
        if self.display_text:
            print('Did add label')
            self.label = Label(
                text=self.text_content.format(self.pb_value), 
                size=(100, 20), 
                size_hint=(None, None),
                color=self.text_color, 
                pos=(5, 5),
                valign='middle', 
                halign='center'
            )
            self.add_widget(self.label)
        self.initialized = True
        self.on_pb_value(self, self.pb_value)

    
    def on_pb_value(self, instance, value):
        if self.initialized:
            pct_filled = value / self.max_value
            width, height = 94, 16
            self.inner_rect.size = (floor(width * pct_filled), height)
            if self.label:
                self.label.text = str(value)

class Panel(BoxLayout):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        with self.canvas.before:
            Color(0.87, 0.67, .4)
            Rectangle(pos=self.pos, size=self.size)