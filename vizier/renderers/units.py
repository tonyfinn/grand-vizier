from random import randint
from typing import Dict, List, Optional, Set, Union, Tuple, TYPE_CHECKING

from kivy.animation import Animation
from kivy.graphics.context_instructions import Color
from kivy.graphics.instructions import Instruction, InstructionGroup
from kivy.graphics.vertex_instructions import Rectangle
from kivy.properties import NumericProperty
from kivy.uix.widget import Widget

from .scroll import Scrollable
from .render_state import MapScrollInfo, MapWidgetPos
from ..game import Vizier
from ..map import MapPos, Path
from ..units import Unit, UnitVisibility

import pdb


class EvilAnimationWrapper(Widget):
    x = NumericProperty()
    y = NumericProperty()

    def __init__(self, instruction):
        super().__init__()
        self.instruction = instruction
        self.x = instruction.pos[0]
        self.y = instruction.pos[1]

    def on_x(self, instance, new_x):
        self.instruction.pos = (new_x, self.instruction.pos[1])

    def on_y(self, instance, new_y):
        self.instruction.pos = (self.instruction.pos[0], new_y)

class UnitView(Scrollable):
    def __init__(self, game: Vizier, scroll_state: MapScrollInfo) -> None:
        super().__init__(scroll_state=scroll_state)
        self.color_instructions = {} # type: Dict[int, Color]
        self.unit_instructions = {} # type: Dict[int, Instruction]
        self.unit_cache = {} # type: Dict[int, Unit]
        self.visible_units = set() # type: Set[int]
        self.instructions = InstructionGroup()
        self.game = game
        game.bind(units=self.on_unit_change)
        self.scrollpane.canvas.add(self.instructions)
        for unit in self.game.units:
            self.add_unit(unit)

    def on_unit_change(self, game: Vizier, units: List[Unit]) -> None:
        print('Unit change {}'.format(units))
        rendered_ids = self.unit_instructions.keys()
        active_ids = set(unit.id for unit in units)
        to_remove = rendered_ids - active_ids
        to_add = active_ids - rendered_ids
        print('Active: {}, Rendered: {}, To Add: {}, To Remove: {}'.format(
            active_ids,
            rendered_ids,
            to_add,
            to_remove))
        for unit_id in to_remove:
            self.remove_unit(self.unit_cache[unit_id])
        
        for unit in units:
            if unit.id in to_add:
                self.add_unit(unit)

    def handle_visiblity_change(self, unit: Unit, updated_property: Union[UnitVisibility, Tuple[float, MapPos], MapPos]) -> None:
        if unit.hp <= 0:
            return
        if isinstance(updated_property, MapPos) and unit.visibility == UnitVisibility.Visible:
            self.unit_moved(unit, updated_property)
        elif isinstance(updated_property, UnitVisibility):
            color_instr = self.color_instructions[unit.id]
            color_instr.a = 0.5 if unit.visibility == UnitVisibility.LastKnown else 1
            instr = self.unit_instructions[unit.id]
            if unit.visibility != UnitVisibility.Invisible and unit.id not in self.visible_units:
                self.visible_units.add(unit.id)
                instr.pos = self.get_render_pos_for_unit(unit)
                self.instructions.add(color_instr)
                self.instructions.add(instr)
            elif unit.visibility == UnitVisibility.Invisible and unit.id in self.visible_units:
                self.visible_units.remove(unit.id)
                self.instructions.remove(color_instr)
                self.instructions.remove(instr)
        elif isinstance(updated_property, tuple) and unit.visibility == UnitVisibility.LastKnown:
            pos = unit.last_known_pos[1]
            instr = self.unit_instructions[unit.id]
            instr.pos = self.get_render_pos_for_unit(unit)


    def get_render_pos_for_unit(self, unit: Unit) -> Optional[Tuple[int, int]]:
        if unit.visibility == UnitVisibility.Invisible:
            return None
        if unit.visibility == UnitVisibility.Visible:
            pos = unit.pos
            mwpos = self.scroll_state.map_pos_to_widget_pos(unit.pos)
        elif unit.visibility == UnitVisibility.LastKnown:
            if unit.last_known_pos is None:
                return None
            pos = unit.last_known_pos[1]
        mwpos = self.scroll_state.map_pos_to_widget_pos(pos)
        return self.apply_offset_based_on_tile(mwpos, pos)

    def remove_unit(self, unit: Unit) -> None:
        unit.unbind(pos=self.unit_moved)
        instruction = self.unit_instructions[unit.id]
        del self.unit_instructions[unit.id]
        del self.color_instructions[unit.id]
        del self.unit_cache[unit.id]
        self.instructions.remove(instruction)

    def apply_offset(self, mwpos, x=None, y=None):
        x_offset = x if x else randint(0, 30)
        y_offset = y if y else randint(0, 30)
        return mwpos.x + x_offset, mwpos.y + y_offset

    def add_unit(self, unit: Unit) -> None:
        mwpos = self.scroll_state.map_pos_to_widget_pos(unit.pos)
        new_pos = self.apply_offset(mwpos)
        print('Rendering {!r} at size {!r} at pos {!r}'.format(unit.icon, Unit.UNIT_SIZE, new_pos))
        alpha = 0.5 if unit.visibility == UnitVisibility.LastKnown else 1
        color_instruction = Color(1, 1, 1, alpha)
        instruction = Rectangle(source=unit.icon, size=Unit.UNIT_SIZE, pos=new_pos)
        if unit.visibility != UnitVisibility.Invisible:
            self.instructions.add(color_instruction)
            self.instructions.add(instruction)
            self.visible_units.add(unit.id)
        self.color_instructions[unit.id] = color_instruction
        self.unit_instructions[unit.id] = instruction
        self.unit_cache[unit.id] = unit
        unit.bind(pos=self.handle_visiblity_change, visibility=self.handle_visiblity_change, last_known_pos=self.handle_visiblity_change)

    def apply_offset_based_on_tile(self, mwpos: MapWidgetPos, pos: MapPos) -> Tuple[int, int]:
        if isinstance(self.game.map.get_tile(pos), Path):
            x, y = mwpos.x + 20, mwpos.y + 24
        else:
            x, y = self.apply_offset(mwpos)
        return x, y

    def unit_moved(self, unit: Unit, pos: MapPos) -> None:
        instr = self.unit_instructions[unit.id]
        mwpos = self.scroll_state.map_pos_to_widget_pos(pos)
        x, y = self.apply_offset_based_on_tile(mwpos, pos)
        anim = Animation(x=x, y=y, duration=0.5)
        anim.start(EvilAnimationWrapper(instr))