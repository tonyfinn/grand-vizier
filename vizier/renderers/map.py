from kivy.uix.floatlayout import FloatLayout
from kivy.uix.relativelayout import RelativeLayout
from kivy.graphics.instructions import InstructionGroup
from kivy.graphics.vertex_instructions import Rectangle
from kivy.core.window import Window
from kivy.properties import ObjectProperty

from typing import Any, NamedTuple, Optional, Tuple
from ..map import Map, MapPos
from ..game import Vizier
from .render_state import MapScrollInfo, WindowPos, TILE_SIZE
from .scroll import Scrollable
from .ui_widgets import PopupPanelLayer
from .map_ui import HasPopupPanel
  

class MapWidget(Scrollable):

    tile_under_cursor = ObjectProperty()

    def __init__(self, game: Vizier, map: Map, scroll_state: MapScrollInfo) -> None:
        super().__init__(scroll_state=scroll_state)
        self.game = game
        self.keyboard = Window.request_keyboard(lambda x: x, self, 'text')
        self.keyboard.bind(on_key_down=self.on_key_down)
        self.popup_layer = PopupPanelLayer()
        Window.bind(mouse_pos=self.on_mouse_move)
        self.map = map
        self.tiles = InstructionGroup()
        self.cursor = Rectangle(pos=(0, 0), source='assets/select.png', size=(TILE_SIZE, TILE_SIZE))
        self.tile_under_cursor = None # type: Optional[MapPos]

        for tile in self.map.tiles:
            wpos = self.scroll_state.map_pos_to_widget_pos(tile.pos)
            self.tiles.add(Rectangle(pos=(wpos.x, wpos.y), source=tile.image, size=(TILE_SIZE, TILE_SIZE)))
        self.scrollpane.canvas.add(self.tiles)


    def on_mouse_move(self, evt_type: Any, pos: Tuple[int, int]) -> None:
        wpos = WindowPos(x=pos[0], y=pos[1])
        mwpos = self.scroll_state.window_pos_to_widget_pos(wpos)
        mpos = self.scroll_state.widget_pos_to_map_pos(mwpos)
        target_pos = self.scroll_state.map_pos_to_widget_pos(mpos)
        if 0 <= mpos.x < self.map.width and 0 <= mpos.y < self.map.height:
            if mpos != self.tile_under_cursor:
                self.tile_under_cursor = mpos

            self.cursor.pos = (target_pos.x, target_pos.y)
            self.scrollpane.canvas.add(self.cursor)
        else:
            self.scrollpane.canvas.remove(self.cursor)


    def on_key_down(self, keyboard, keycode, text, modifiers):
        key = keycode[1]
        if key == 'down':
            self.scroll_state.scroll(0, 20)
        elif key == 'up':
            self.scroll_state.scroll(0, -20)
        elif key == 'left':
            self.scroll_state.scroll(20, 0)
        elif key == 'right':
            self.scroll_state.scroll(-20, 0)

    def on_touch_down(self, touch):
        super().on_touch_down(touch)
        if 'button' in touch.profile:
            clicked_tile = self.scroll_state.window_pos_to_map_pos(WindowPos(touch.x, touch.y))
            print('Clicked at {}'.format(clicked_tile))
            popups = []
            for popup_source in HasPopupPanel.popup_producers:
                if popup_source.pos == clicked_tile:
                    print('Click on {}'.format(popup_source))
                    popups.append(popup_source)
            if len(popups) == 1:
                self.popup_layer.add_popup_panel(popups[0])
            elif len(popups) == 0:
                print('No popups found for: {} (all: {})'.format(clicked_tile, HasPopupPanel.popup_producers))
        return False

    def on_touch_up(self, touch):
        super().on_touch_up(touch)
        if 'button' in touch.profile:
            return False
            print('{!r}: {}'.format(touch, touch.button))
            