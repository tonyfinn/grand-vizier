from kivy.event import EventDispatcher
from kivy.properties import NumericProperty

from typing import NamedTuple
from ..map import MapPos


TILE_SIZE = 64


class MapWidgetPos(NamedTuple):
    x: int
    y: int


class WindowPos(NamedTuple):
    x: int
    y: int


class MapScrollInfo(EventDispatcher):
    scroll_x = NumericProperty(0)
    scroll_y = NumericProperty(0)

    instance = None # type: MapScrollInfo

    def __init__(self, map):
        MapScrollInfo.instance = self
        self.map = map

    def map_pos_to_widget_pos(self, mpos: MapPos) -> MapWidgetPos:
        y = ((self.map.height - 1) - mpos.y) * TILE_SIZE
        return MapWidgetPos(x=(mpos.x * TILE_SIZE), y=y)

    def widget_pos_to_window_pos(self, mwpos: MapWidgetPos) -> WindowPos:
        return WindowPos(x=mwpos.x + self.scroll_x, y=mwpos.y + self.scroll_y)

    def window_pos_to_widget_pos(self, wpos: WindowPos) -> MapWidgetPos:
        return MapWidgetPos(x=wpos.x - self.scroll_x, y=wpos.y - self.scroll_y)

    def widget_pos_to_map_pos(self, mwpos: MapWidgetPos) -> MapPos:
        x = mwpos.x // TILE_SIZE
        y = (self.map.height - 1) - (mwpos.y // TILE_SIZE)
        return MapPos(x=int(x), y=int(y))

    def window_pos_to_map_pos(self, wpos: WindowPos) -> MapPos:
        return self.widget_pos_to_map_pos(self.window_pos_to_widget_pos(wpos))

    def map_pos_to_window_pos(self, mpos: MapPos) -> WindowPos:
        return self.widget_pos_to_window_pos(self.map_pos_to_widget_pos(mpos))

    def scroll(self, x: int, y: int) -> None:
        self.scroll_x += x
        self.scroll_y += y