from typing import List, Tuple, TYPE_CHECKING

from kivy.event import EventDispatcher
from kivy.graphics.context_instructions import Color
from kivy.graphics.vertex_instructions import Rectangle
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.label import Label

if TYPE_CHECKING:
    from ..map import MapPos
    from .render_state import MapScrollInfo

        
class PopupPanel(BoxLayout):
    def __init__(self, scroll_state: 'MapScrollInfo', pos: Tuple[int, int], size: Tuple[int, int], *args, **kwargs) -> None:
        popup_pos = scroll_state.map_pos_to_window_pos(pos)
        super().__init__(pos=(popup_pos.x, popup_pos.y), size=size, size_hint = (None, None), orientation='vertical', *args, **kwargs)
        self.register_event_type('on_close')
        scroll_state.bind(scroll_x=self.close, scroll_y=self.close)
        with self.canvas.before:
            Color(0.5, 0.5, 0.5, 0.75)
            Rectangle(pos=popup_pos, size=size)
            Color(1, 1, 1, 0.75)

    def on_close(self): pass

    def close(self, instance, value):
        self.dispatch('on_close')


class PopupButton(Button):
    def __init__(self, text, target, key):
        super(Button, self).__init__(text=text, size=(100, 30))
        self.target = target
        self.key = key

    def on_press(self):
        self.target.dispatch('on_' + self.key)
        self.target.update_labels()
        super().on_press()


class PopupAdjustButton(Button):
    ADJUST_MINUS = '-'
    ADJUST_PLUS = '+'

    def __init__(self, adjustment, target, key):
        super(Button, self).__init__(text=adjustment, size=(100, 30))
        self.adjustment = adjustment
        self.target = target
        self.key = key

    def on_press(self):
        current_value = getattr(self.target, self.key)
        if self.adjustment == PopupAdjustButton.ADJUST_MINUS and current_value <= 0:
            super().on_press()
            return
        if self.adjustment == PopupAdjustButton.ADJUST_MINUS:
            new_value = current_value - 1
        elif self.adjustment == PopupAdjustButton.ADJUST_PLUS:
            new_value = current_value + 1
        setattr(self.target, self.key, new_value)
        self.target.dispatch('on_' + self.key, current_value, new_value)
        self.target.update_adjustables()
        super().on_press()


class HasPopupPanel(EventDispatcher):
    pos: 'MapPos'
    popup_title: str
    popup_actions: List[Tuple[str, str]] # (text, key)
    popup_metrics: List[Tuple[str, str]] # (text, attr)
    popup_adjustables: List[Tuple[str, str]]
    popup_producers = [] # type: List[HasPopupPanel]
    next_popup_id = 0

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        print('Adding popup producer: {}'.format(self))
        self.popup_source_id = HasPopupPanel.next_popup_id
        self.popup_panel = None
        self.popup_open = False        
        self._popup_labels: List[Label] = []
        self._popup_adjust_labels: List[Label] = []
        HasPopupPanel.next_popup_id += 1
        HasPopupPanel.popup_producers.append(self)

    def build_popup_panel(self) -> PopupPanel:
        from .render_state import MapScrollInfo
        scroll_state = MapScrollInfo.instance
        height = 30 * (
            1 # Title Label
            + len(self.popup_metrics)
            + len(self.popup_actions)
            + (2 * len(self.popup_adjustables))
        )
        self.popup_panel = PopupPanel(scroll_state, self.pos, size=(150, height))

        self.populate_panel()
        self.popup_open = True
        return self.popup_panel

    def popup_closed(self):
        self.popup_open = False
        self.popup_panel = None        
        self._popup_labels: List[Label] = []
        self._popup_adjust_labels: List[Label] = []

    def populate_panel(self):
        self.popup_panel.add_widget(Label(text=self.popup_title, size=(150, 30)))
        for text, key in self.popup_metrics:
            label = Label(text=text.format(getattr(self, key)))
            self._popup_labels.append(label)
            self.popup_panel.add_widget(label)
        for text, key in self.popup_actions:
            self.popup_panel.add_widget(PopupButton(text, self, key))
        for text, key in self.popup_adjustables:
            label = Label(text=text.format(getattr(self, key)))
            self._popup_adjust_labels.append(label)
            minus_button = PopupAdjustButton('-', self, key)
            plus_button = PopupAdjustButton('+', self, key)
            button_layout = BoxLayout(orientation='horizontal', size=(150, 30))
            button_layout.add_widget(minus_button)
            button_layout.add_widget(plus_button)
            self.popup_panel.add_widget(label)
            self.popup_panel.add_widget(button_layout)

    def update_labels(self):
        if not self.popup_open:
            return
        print('Update labels')
        for i, (text, key) in enumerate(self.popup_metrics):
            target_text = text.format(getattr(self, key))
            self._popup_labels[i].text = target_text

    def update_adjustables(self):
        if not self.popup_open:
            return
        print('Update labels')
        for i, (text, key) in enumerate(self.popup_adjustables):
            target_text = text.format(getattr(self, key))
            self._popup_adjust_labels[i].text = target_text