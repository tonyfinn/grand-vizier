from kivy.uix.floatlayout import FloatLayout
from kivy.uix.relativelayout import RelativeLayout

from .render_state import MapScrollInfo

from typing import Tuple


class Scrollable(FloatLayout):
    def __init__(self, scroll_state: MapScrollInfo, **kwargs) -> None:
        super().__init__(**kwargs)
        self.scroll_state = scroll_state
        self.scroll_state.bind(scroll_x=self.on_scroll, scroll_y=self.on_scroll)
        self.scrollpane = RelativeLayout(size=(1, 1), pos=(0, 0))
        self.add_widget(self.scrollpane)

    def on_scroll(self, scroll_state: MapScrollInfo, value: int) -> None:
        self.scrollpane.pos = (scroll_state.scroll_x, scroll_state.scroll_y)