from math import floor
from typing import List, Tuple

from kivy.graphics.context_instructions import Color
from kivy.graphics.vertex_instructions import Rectangle
from kivy.properties import BooleanProperty, NumericProperty, ObjectProperty
from kivy.uix.button import Button
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.label import Label

from ..game import Vizier
from ..utils import calculate_calendar_date
from .map import MapScrollInfo
from .ui_widgets import Panel, PopupPanelLayer, VzProgressBar


class Notification(BoxLayout):
    def __init__(self, text):
        super().__init__(size=(500, 50), size_hint=(None, None), orientation='horizontal')
        self.register_event_type('on_dismiss')
        with self.canvas.before:
            Color(0, 0, 0, a=0.75)
            self.bg = Rectangle(size=(500, 50), pos=self.pos)
            Color(1, 1, 1, a=1)
        
        self.bind(pos=self.on_move)
        close_button = Button(text='X', size=(20, 50), size_hint=(None, None), on_press=lambda button: self.dispatch('on_dismiss'))
        text_label = Label(text=text, size=(480, 50), text_size=(470, 50), valign='middle', size_hint=(None, None))
        self.add_widget(text_label)
        self.add_widget(close_button)

    def on_dismiss(self) -> None:
        pass

    def on_move(self, instance, pos):
        self.bg.pos = pos


class NotificationPanel(BoxLayout):
    def __init__(self) -> None:
        super().__init__(size=(500, 50), orientation='vertical', pos=(390, 670))
        self.notifications: List[Notification] = []

    def add_notification(self, text: str) -> None:
        notification = Notification(text)
        self.notifications.append(notification)
        self.resize_for_notifications()
        self.add_widget(notification)
        notification.bind(on_dismiss=self.dismiss)

    def resize_for_notifications(self) -> None:
        self.pos = (390, 720 - (len(self.notifications) * 50))
        self.size = (500, len(self.notifications) * 50)
    
    def dismiss(self, notification: Notification) -> None:
        notification.unbind(on_dismiss=self.dismiss)
        self.remove_widget(notification)
        self.notifications.remove(notification)
        self.resize_for_notifications()

class UIRenderer(FloatLayout):
    def __init__(self, game: Vizier) -> None:
        super().__init__()
        self.game = game
        self.unrest_progress_bar = VzProgressBar(pb_value=self.game.global_unrest, fg_color=(1, 0, 0), bg_color=(0, 1, 0), size=(100, 20))
        self.unrest_label = Label(text='Unrest', size_hint=(None, None), size=(70, 30), valign='middle', color=(0, 0, 0, 1))
        self.game.bind(global_unrest=self.update_unrest)
        self.game.bind(gold=self.update_gold)
        self.game.bind(elapsed_time=self.update_clock)
        self.game.bind(on_notification=self.handle_notification)
        self.panel = Panel(orientation='horizontal', size=(1280, 30))
        self.panel.add_widget(self.unrest_label)
        self.panel.add_widget(self.unrest_progress_bar)
        self.calendar_label = Label(
            text='Time: ' + calculate_calendar_date(0.0),
            size_hint=(None, None),
            size=(120, 30), 
            valign='middle', 
            color=(0, 0, 0, 1)
        )
        self.gold_label = Label(text='Gold: 10g', size_hint=(None, None), size=(70, 30), valign='middle', color=(0, 0, 0, 1))
        self.cursor_label = Label(text='Tile: None', size_hint=(None, None), size=(100, 30), valign='middle', color=(0, 0, 0, 1))
        self.panel.add_widget(self.cursor_label)
        self.panel.add_widget(self.gold_label)
        self.panel.add_widget(self.calendar_label)
        self.notification_panel = NotificationPanel()
        self.add_widget(self.notification_panel)
        self.add_widget(self.panel)

    def update_cursor(self, pos):
        label = 'None' if pos is None else '({}, {})'.format(pos.x, pos.y)
        self.cursor_label.text = 'Tile: ' + label

    def update_unrest(self, game: Vizier, new_unrest: int) -> None:
        self.unrest_progress_bar.pb_value = new_unrest

    def update_gold(self, game: Vizier, new_gold: int) -> None:
        self.gold_label.text = 'Gold: {}g'.format(new_gold)

    def update_clock(self, game: Vizier, elapsed_time: float) -> None:
        self.calendar_label.text = 'Time: ' + calculate_calendar_date(elapsed_time)

    def handle_notification(self, game: Vizier, notification: str) -> None:
        self.notification_panel.add_notification(notification)