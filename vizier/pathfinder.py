from bisect import insort, bisect_left, bisect_right
from collections import defaultdict

from typing import Dict, List, Set, TYPE_CHECKING

from vizier.map import Map, MapPos, MapTile

class NoPathException(Exception):
    pass


class ScoreLookup:
    def __init__(self):
        self.scores = []
        self.score_values = {}
        self.scores_reverse = defaultdict(list)

    def __getitem__(self, cell):
        return self.score_values[cell]

    def __setitem__(self, cell, score):
        self.score_values[cell] = score
        self.scores_reverse[score].append(cell)
        if not self.score_existing(score):
            self.scores.append(score)

    def __delitem__(self, cell):
        score = self[cell]
        del self.score_values[cell]
        self.scores_reverse[score].remove(cell)
        if len(self.scores_reverse[score]) == 0:
            self.scores.remove(score)

    def score_existing(self, score):
        l = bisect_left(self.scores, score)
        r = bisect_right(self.scores, score)
        return score in self.scores[l:r]

    def index(self, score):
        l = bisect_left(self.scores, score)
        r = bisect_right(self.scores, score)
        return self.scores[l:r].index(score) + l


def contact_neighbours(pos: MapPos) -> List[MapPos]:
    x, y = pos.x, pos.y
    return [MapPos(x=x + 1, y=y), MapPos(x=x - 1, y=y), MapPos(x=x, y=y + 1), MapPos(x=x, y=y - 1)]


def in_map(map: Map, pos: MapPos) -> bool:
    return 0 <= pos.x < map.width and 0 <= pos.y < map.height


def constrain_points_to_map(map: Map, positions: List[MapPos]) -> List[MapPos]:
    return [pos for pos in positions if in_map(map, pos)]


class PathFinder:

    def __init__(self, start: MapPos, end: MapPos, map: Map) -> None:
        self.start = start
        self.end = end
        self.map = map
        self.closed_cells = set() # type: Set[MapPos]
        self.open_cells = set() # type: Set[MapPos]
        self.open_cells.add(start)
        self.f_score = ScoreLookup()
        self.g_score = ScoreLookup()
        self.came_from = {} # type: Dict[MapPos, MapPos]

    def valid_neighbours(self, pos: MapPos, consider_obstacles: bool) -> List[MapPos]:
        all_neighbours = constrain_points_to_map(self.map, contact_neighbours(pos))
        tile = self.map.get_tile(pos)
        if not consider_obstacles:
            return all_neighbours
        def tiles_connect(t1: MapTile, t2: MapTile) -> bool:
            return t1.connects_with(t2) and t2.connects_with(t1)
        return [x for x in all_neighbours if tiles_connect(tile, self.map.get_tile(x))]

    def find_path(self, consider_obstacles=True) -> List[MapPos]:
        self.g_score[self.start] = 0

        self.f_score[self.start] = self.cost_estimate(self.start, self.end)

        while len(self.open_cells) > 0:
            current_node = sorted(self.open_cells, key=lambda c: self.f_score[c])[0]
            if current_node == self.end:
                return self.build_path(self.came_from, self.end)
            self.open_cells.remove(current_node)
            self.closed_cells.add(current_node)
            for neighbour in self.valid_neighbours(current_node, consider_obstacles):
                if neighbour in self.closed_cells:
                    continue
                temp_g_score = self.g_score[current_node] + 1
                if neighbour not in self.open_cells or temp_g_score < self.g_score[neighbour]:
                    self.came_from[neighbour] = current_node
                    self.g_score[neighbour] = temp_g_score
                    self.f_score[neighbour] = self.g_score[neighbour] + self.cost_estimate(neighbour, self.end)
                    if neighbour not in self.open_cells:
                        self.open_cells.add(neighbour)
        raise NoPathException

    @staticmethod
    def cost_estimate(start: MapPos, end: MapPos) -> int:
        return abs(end.x - start.x) + abs(end.y - start.y)

    @classmethod
    def build_path(cls, came_from: Dict[MapPos, MapPos], node: MapPos) -> List[MapPos]:
        path: List[MapPos] = []
        if node in came_from:
            early_path = cls.build_path(came_from, came_from[node])
            early_path.append(node)
            return early_path
        else:
            return [node]

    @staticmethod
    def simplify_path(path: List[MapPos]) -> List[MapPos]:
        simple_path = [path[0]]
        segment_start = path[0]
        last_checked = segment_start
        for tile in path:
            x, y = tile
            if segment_start[0] == x or segment_start[1] == y:
                last_checked = tile
                continue
            simple_path.append(last_checked)
            segment_start = last_checked
            last_checked = tile
        if len(path) > 1:
            simple_path.append(last_checked)
        return simple_path