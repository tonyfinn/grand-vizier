import pdb
import random

from enum import Enum
from random import randint
from typing import List, Optional, Tuple, TYPE_CHECKING

from kivy.event import EventDispatcher
from kivy.properties import ObjectProperty

from vizier.map import MapPos, Map, Village
from vizier.pathfinder import PathFinder
from vizier.utils import calculate_calendar_date

if TYPE_CHECKING:
    from .game import Vizier


class HostileActionReason(Enum):
    Agitate = 'raise unrest'
    Train = 'train'
    Heal = 'heal'
    Raid = 'end your rule'


class UnitOwner(Enum):
    Player = 0
    AI = 1


class UnitVisibility(Enum):
    Invisible = 0
    LastKnown = 1
    Visible = 2


class Unit(EventDispatcher):
    icon: str
    UNIT_SIZE = (30, 64)
    pos = ObjectProperty() # type: MapPos
    next_character_id = 0
    visibility = ObjectProperty()
    last_known_pos = ObjectProperty() # type: Optional[Tuple[float, MapPos]]
    def __init__(self, game: 'Vizier', map: Map, pos: MapPos, owner: UnitOwner) -> None:
        self.register_event_type('on_death')
        self.visibility = UnitVisibility.Visible if owner == UnitOwner.Player else UnitVisibility.Invisible
        self.id = self.next_character_id
        self.been_attacked = False
        self.game = game
        Unit.next_character_id += 1
        self.pos = pos
        self.owner = owner
        self.map = map
        self.target_path = None # type: Optional[List[MapPos]]
        self.hp = 10
        self.attack = 5
        self.is_engaged = False

    def __str__(self):
        return '{}#{} @ {}'.format(self.__class__.__name__, self.id, self.pos)

    def __repr__(self):
        return '{}(id={}, pos={})'.format(self.__class__.__name__, self.id, self.pos)

    def tick(self, dt: float) -> None:
        pass

    def attack_in_tile(self):
        enemies = self.find_hostiles_in_tile()
        if len(enemies) == 0:
            return
        next_enemy = enemies.pop()
        print('Next enemy: {}'.format(next_enemy))
        while self.hp > 0 and next_enemy:
            next_enemy.been_attacked = True
            self.fight(next_enemy)
            if len(enemies) > 0:
                next_enemy = enemies.pop()
            else:
                next_enemy = None

    def fight(self, other: 'Unit') -> None:
        print('{} is fighting {}'.format(self, other))
        tile = self.map.get_tile(self.pos)
        while True:
            other.hp -= self.attack
            if other.hp > 0:
                self.hp -= other.attack                
            else:
                self.hp = max(1, self.hp - other.attack)
                other.dispatch('on_death')
                victor = self
                loser = other
                break
            if self.hp <= 0:
                self.dispatch('on_death')
                victor = other
                loser = self
                break
        if loser in tile.stationed_units:
            tile.stationed_units.remove(loser)

        self.game.fire_notification('A {} has slain a {} at {} with {}/{} HP remaining'.format(
            victor.__class__.__name__.lower(),
            loser.__class__.__name__.lower(),
            tile.get_notification_name(),
            victor.hp,
            victor.max_hp
        ))

    def find_hostiles_in_tile(self):
        tile = self.map.get_tile(self.pos)
        return [unit for unit in tile.stationed_units if unit.owner != self.owner]

    def goto(self, dest: MapPos) -> None:  
        path = PathFinder(self.pos, dest, self.map).find_path(True)
        # simple_path = PathFinder.simplify_path(path)
        # TODO: Use simple path for smoother animations
        self.target_path = path[1:]
        self.target_path.reverse()

    def follow_path(self) -> None:
        if self.target_path is None:
            return
        if len(self.target_path) == 0:
            self.arrived_at_target()
            self.target_path = None
        else:
            self.pos = self.target_path.pop()

    def arrived_at_target(self) -> None:
        pass

    def on_death(self) -> None:
        pass

class Action:
    def __init__(self, taker: Unit, src: MapPos, dest: MapPos, action: HostileActionReason, when: float) -> None:
        self.taker = taker
        self.src = src
        self.dest = dest
        self.action = action
        self.when = when

    def __str__(self):
        return '{} went from ({}, {}) to {} at {} to {}.'.format(
            'The hero' if isinstance(self.taker, Hero) else 'Rebels',
            self.src.x, self.src.y,
            self.taker.map.get_tile(self.dest).get_notification_name(),
            calculate_calendar_date(self.when),
            self.action.value
        )


class HeroState(Enum):
    Train = 0
    Heal = 1
    MovingVillage = 2
    MovingCastle = 3
    Idle = 4
    Attacking = 5


class Hero(Unit):
    icon = 'assets/hero-villager.png'
    def __init__(self, game: 'Vizier', map: Map, pos: MapPos) -> None:
        super().__init__(game, map, pos, UnitOwner.AI)
        self.register_event_type('on_game_over')
        self.target_village = None # type: Optional[Village]
        self.current_village = self.game.map.get_tile(pos)
        self.current_village.stationed_units.append(self)
        self.target_path = None # type: List[MapPos]
        self.movement_ready = True
        self.level = 1
        self.max_hp = 30
        self.hp = 30
        self.attack = 10
        self.state = HeroState.Idle
        self.train_ticks = 30

    def tick(self, dt: float) -> None:
        self.last_target = None
        if self.been_attacked or (self.level < 10 and self.state == HeroState.Idle):
            possible_villages = [village for village in self.map.villages
                                        if village.pos != self.pos
                                        and village is not self.last_target]
            self.target_village = random.choice(self.map.villages)
            print('Chosen village: {}'.format(self.target_village))
            if self in self.current_village.stationed_units:
                self.current_village.stationed_units.remove(self)
            self.current_village.events.append(Action(
                self,
                self.pos,
                self.target_village.pos,
                HostileActionReason.Train,
                self.game.elapsed_time
            ))
            self.goto(self.target_village.pos)
            self.state = HeroState.MovingVillage
            if self.visibility == UnitVisibility.Visible:
                self.visibility = UnitVisibility.LastKnown
        elif self.level >= 10 and self.state == HeroState.Idle:
            self.state = HeroState.MovingCastle
            self.goto(self.map.castle.pos)
            if self.visibility == UnitVisibility.Visible:
                self.visibility = UnitVisibility.LastKnown
        elif self.state in (HeroState.MovingVillage, HeroState.MovingCastle):   
            self.follow_path()
        elif self.state == HeroState.Train:
            self.train_ticks -= 1
            if self.train_ticks <= 0:
                self.level += 1
                self.state = HeroState.Idle

    def arrived_at_target(self):
        if self.state == HeroState.MovingVillage:
            self.current_village = self.target_village
            self.current_village.stationed_units.append(self)
            self.train_ticks = 20
            self.last_target = self.target_village
            self.target_village = None
        elif self.state == HeroState.MovingCastle:
            self.attack_in_tile()
            self.dispatch('on_game_over')

    def on_game_over(self):
        pass


class RebelState(Enum):
    Idle = 0
    Agitating = 1
    Moving = 2
    MovingCastle = 3
    FightingCastle = 4

class Rebel(Unit):
    icon = 'assets/rebel-villager.png'
    def __init__(self, game: 'Vizier', map: Map, pos: MapPos) -> None:
        super().__init__(game, map, pos, UnitOwner.AI)
        self.last_known_pos = game.elapsed_time, self.pos
        self.visibility = UnitVisibility.LastKnown
        self.target_village = None # type: Optional[Village]
        self.current_village = self.map.get_tile(self.pos)
        self.state = RebelState.Idle
        self.agitate_ticks = 30
        self.max_hp = 3
        self.hp = 3

    def pick_target_village(self):
        weighted_villages = [(max(10, 100 - village.unrest), village) 
                                for village in sorted(self.map.villages, key=lambda v: min(10, 100 - v.unrest))
                                if village.pos != self.pos]
        max_weight = sum(weight for weight, _ in weighted_villages)
        choice = randint(0, max_weight)
        cum_sum = 0
        for weight, village in weighted_villages:
            cum_sum += weight
            if choice <= cum_sum:
                return village

    def attack_castle(self):
        self.state = RebelState.MovingCastle
        self.goto(self.map.castle.pos)           
        self.current_village.events.append(Action(
            self,
            self.current_village.pos,
            self.map.castle.pos, 
            HostileActionReason.Raid, 
            self.game.elapsed_time))

    def attack_random_village(self):
        self.target_village = self.pick_target_village()
        self.current_village.events.append(Action(
            self,
            self.current_village.pos,
            self.target_village.pos, 
            HostileActionReason.Agitate, 
            self.game.elapsed_time))
        self.goto(self.target_village.pos)
        self.state = RebelState.Moving

    def tick(self, dt: float) -> None:
        self.last_target = None
        if self.state == RebelState.Idle:
            action_roll = randint(0, 100)
            if action_roll < 10:
                self.attack_castle()
            else:
                self.attack_random_village()
        elif self.state in (RebelState.Moving, RebelState.MovingCastle):  
            self.follow_path()
        else:
            self.agitate_ticks -= 1
            if self.agitate_ticks <= 0:
                self.current_village.unrest += 5
                self.last_known_pos = self.game.elapsed_time, self.pos
                self.state = RebelState.Idle

    def arrived_at_target(self):
        if self.state == RebelState.Moving:
            self.current_village = self.target_village
            self.current_village.stationed_units.append(self)
            self.target_village = None
            self.state = RebelState.Agitating
            self.agitate_ticks = 20
            self.attack_in_tile()
        elif self.state == RebelState.MovingCastle:
            print('Arrived at the castle!')
            self.attack_in_tile()
            if self.hp > 0:
                self.game.dispatch('on_game_over')


class SoldierState(Enum):
    Home = 0
    Village = 1
    MovingVillage = 2
    MovingHome = 3
    VillageActing = 4


class Spy(Unit):
    icon = 'assets/spy-villager.png'
    def __init__(self, game: 'Vizier', map: Map, pos: MapPos) -> None:
        super().__init__(game, map, pos, UnitOwner.Player)
        self.state = SoldierState.Home
        self.current_village: Optional[Village] = None
        self.spy_ticks = 20
        self.hp = 10
        self.max_hp = 10
        
    def find_most_needed_village(self) -> Optional[Village]:
        needy_villages = sorted((village for village in self.map.villages
                                if village.spy_neediness > 0),
                                key=lambda v: v.spy_neediness)
        if len(needy_villages) > 0:
            return needy_villages[0]
        else:
            return None

    def reinforce_village(self, village: Village) -> None:
        self.target_village = village
        self.target_village.pending_spy_count += 1
        self.state = SoldierState.MovingVillage
        self.goto(self.target_village.pos)

    def tick(self, dt):
        if self.state == SoldierState.Home:
            village_in_need = self.find_most_needed_village()
            if village_in_need:
                self.reinforce_village(village_in_need)
        elif self.state in (SoldierState.MovingHome, SoldierState.MovingVillage):
            self.follow_path()
        elif self.state == SoldierState.Village:
            cv_neediness = self.current_village.spy_neediness
            most_needy = self.find_most_needed_village()
            if most_needy and (cv_neediness + 1) < most_needy.spy_neediness:
                self.reinforce_village(most_needy)
                self.current_village.stationed_units.remove(self)
            elif cv_neediness < 0:
                self.state = SoldierState.MovingHome
                self.current_village.stationed_units.remove(self)
                self.goto(self.map.castle.pos)
            else:
                self.state = SoldierState.VillageActing
                self.spy_ticks = 20
        elif self.state == SoldierState.VillageActing:
            if self.current_village.spy_neediness < 0:
                self.state = SoldierState.Village
            else:
                self.spy_ticks -= 1
                if self.spy_ticks == 0:
                    if self.current_village.unrest < 100 and len(self.current_village.events) > 0:
                        unrest_modifier = max(self.current_village.unrest, 100)
                        success_chance = randint(0, 200)
                        if success_chance > self.current_village.unrest:
                            event = random.choice(self.current_village.events)
                            if not event.taker.last_known_pos or event.taker.last_known_pos[0] < event.when:
                                event.taker.last_known_pos = event.when, event.dest
                            if ((event.taker.target_village and event.taker.target_village.pos == event.dest)
                                or event.dest == event.taker.pos):
                                event.taker.visibility = UnitVisibility.Visible
                            elif event.taker.visibility == UnitVisibility.Invisible:
                                event.taker.visibility = UnitVisibility.LastKnown
                            self.game.fire_notification(str(event))
                            self.current_village.events.remove(event)
                        else:
                            self.game.fire_notification('Your spy thinks villagers in {} might know something, but they distrust you too much to talk.'.format(self.current_village.name))
                    else:
                        if self.current_village.unrest > 10:
                            self.current_village.unrest = max(10, self.current_village.unrest - 5)
                            self.game.fire_notification('Spy reduced unrest at {}'.format(self.current_village.name))
                        else:
                            self.game.fire_notification('Your spy in {} has found nothing.'.format(self.current_village.get_notification_name()))
                    self.state = SoldierState.Village

    def arrived_at_target(self):
        if self.state == SoldierState.MovingVillage:
            self.current_village = self.target_village
            self.target_village = None
            self.current_village.stationed_units.append(self)
            self.current_village.pending_spy_count -= 1
            self.state = SoldierState.VillageActing
        elif self.state == SoldierState.MovingHome:
            self.map.castle.stationed_units.append(self)
            self.state = SoldierState.Home


class Soldier(Unit):
    icon = 'assets/soldier-placeholder.png'
    def __init__(self, game: 'Vizier', map: Map, pos: MapPos) -> None:
        super().__init__(game, map, pos, UnitOwner.Player)
        self.state = SoldierState.Home
        self.current_village: Optional[Village] = None
        self.max_hp = 10
        self.hp = 10

    def find_most_needed_village(self) -> Optional[Village]:
        needy_villages = sorted((village for village in self.map.villages
                                if village.troop_neediness > 0),
                                key=lambda v: v.troop_neediness)
        if len(needy_villages) > 0:
            return needy_villages[0]
        else:
            return None


    def reinforce_village(self, village: Village) -> None:
        self.target_village = village
        self.target_village.pending_soldier_count += 1
        self.state = SoldierState.MovingVillage
        self.goto(self.target_village.pos)

    def tick(self, dt):
        if self.state == SoldierState.Home:
            village_in_need = self.find_most_needed_village()
            if village_in_need:
                self.reinforce_village(village_in_need)
        elif self.state in (SoldierState.MovingHome, SoldierState.MovingVillage):
            self.follow_path()
        elif self.state == SoldierState.Village and self.find_hostiles_in_tile():
            self.attack_in_tile()
        elif self.state == SoldierState.Village:
            cv_neediness = self.current_village.troop_neediness
            most_needy = self.find_most_needed_village()
            if most_needy and (cv_neediness + 1) < most_needy.troop_neediness:
                self.reinforce_village(most_needy)
                self.current_village.stationed_units.remove(self)
            elif cv_neediness < 0:
                self.state = SoldierState.MovingHome
                self.current_village.stationed_units.remove(self)
                self.goto(self.map.castle.pos)

    def arrived_at_target(self):
        if self.state == SoldierState.MovingVillage:
            self.current_village = self.target_village
            self.target_village = None
            self.current_village.stationed_units.append(self)
            self.current_village.pending_soldier_count -= 1
            self.state = SoldierState.Village
        elif self.state == SoldierState.MovingHome:
            self.map.castle.stationed_units.append(self)
            self.state = SoldierState.Home