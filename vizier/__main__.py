import kivy
kivy.require('1.9.1')

from kivy.app import App
from kivy.config import Config
from kivy.core.window import Window
from kivy.uix.relativelayout import RelativeLayout
from kivy.uix.label import Label
from kivy.uix.popup import Popup

from typing import Any

from vizier.game import Vizier
from vizier.map import MapPos
from vizier.units import Hero
from vizier.renderers.render_state import MapScrollInfo
from vizier.renderers.units import UnitView
from vizier.renderers.map import MapWidget
from vizier.renderers.ui import UIRenderer

import sys

class VizierApp(App):
    def build(self):
        self.layout = RelativeLayout(size=(1, 1))
        self.game = Vizier()
        self.game.bind(on_game_over=self.on_game_over, on_game_win=self.on_game_win)
        self.scroll_state = MapScrollInfo(self.game.map)
        self.ui = UIRenderer(self.game)
        self.map_widget = MapWidget(self.game, self.game.map, self.scroll_state)
        self.map_widget.bind(tile_under_cursor=self.update_cursor)
        self.unit_view = UnitView(self.game, self.scroll_state)
        self.layout.add_widget(self.map_widget)
        self.layout.add_widget(self.unit_view)
        self.layout.add_widget(self.ui)
        self.layout.add_widget(self.map_widget.popup_layer)
        
        return self.layout

    def update_cursor(self, map: MapWidget, pos: Any) -> None:
        self.ui.update_cursor(pos)

    def on_game_over(self, game):
        popup = Popup(title='Game Over',
            content=Label(text='You have been slain!'),
            size_hint=(None, None), size=(600, 600))
        popup.bind(on_dismiss=lambda popup: sys.exit(0))
        popup.open()

    def on_game_win(self, game):
        popup = Popup(title='Victory!',
            content=Label(text='You have killed the hero!'),
            size_hint=(None, None), size=(600, 600))
        popup.bind(on_dismiss=lambda popup: sys.exit(0))
        popup.open()

if __name__ == '__main__':
    Config.set('input', 'mouse', 'mouse,disable_multitouch')
    Window.size = (1280, 720)
    VizierApp().run()