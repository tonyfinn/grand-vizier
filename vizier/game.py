import random

from .units import Unit, Hero, Soldier, Spy
from .map import Map, MapPos

from kivy.clock import Clock
from kivy.event import EventDispatcher
from kivy.properties import ListProperty, NumericProperty

from typing import List

class Vizier(EventDispatcher):
    global_unrest = NumericProperty(10)
    units = ListProperty() # type: List[Unit]
    gold = NumericProperty(10)
    elapsed_time = NumericProperty(0.0)

    def __init__(self) -> None:
        self.map = Map(self)
        self.register_event_type('on_notification')
        self.register_event_type('on_game_over')
        self.register_event_type('on_game_win')
        Clock.schedule_interval(self.tick, 0.5)

        hv = random.choice(self.map.villages)
        print('hv: {}'.format(hv.pos))
        self.hero = Hero(self, self.map, hv.pos)
        self.hero.bind(on_game_over=lambda hero: self.dispatch('on_game_over'))
        self.spawn_unit(self.hero)
        for i in range(2):
            ns = Soldier(self, self.map, self.map.castle.pos)
            self.spawn_unit(ns)
            self.map.castle.stationed_units.append(ns)
        spy = Spy(self, self.map, self.map.castle.pos)
        self.spawn_unit(spy)
        self.map.castle.stationed_units.append(spy)

    def tick(self, dt: float) -> None:
        self.elapsed_time += dt
        for unit in self.units:
            unit.tick(dt)
        for village in self.map.villages:
            village.tick(dt)

    def fire_notification(self, notification: str) -> None:
        print('Notification: {}'.format(str))
        self.dispatch('on_notification', notification)

    def on_notification(self, notification: str) -> None:
        pass

    def on_game_over(self) -> None:
        pass

    def on_game_win(self) -> None:
        pass

    def on_death(self, unit: Unit) -> None:
        if unit in self.units:
            self.units.remove(unit)
        else:
            print('Unit not in Vizier.units: {}'.format(unit))
        if isinstance(unit, Hero):
            self.dispatch('on_game_win')

    def spawn_unit(self, unit: Unit) -> None:
        self.units.append(unit)
        unit.bind(on_death=self.on_death)