import random

from enum import Enum
from typing import List, NamedTuple, overload, Tuple, TYPE_CHECKING
from random import randint

from vizier.renderers.map_ui import HasPopupPanel

from kivy.event import EventDispatcher
from kivy.properties import ListProperty, NumericProperty

if TYPE_CHECKING:
    from .game import Vizier
    from .units import Unit

DEFAULT_MAP_LAYOUT = """
    ..... ..... ..... .....
    ..... ..... ..... .....
    .#--- ----- -#... .....
    ..... ..-.. -=--- ---#.
    ..... ..-.. ..... .-...
    
    ..... ..-.. ..... .-...
    ..... ..-.. ..... .-...
    ..... ----- ----- --#..
    ..... -.#.. ..... .-=..
    ..... -.... ..... ..-..
    
    ..... -.... ..... ..-..
    ..... -.... ..... ..-..
    ..... -.... .---- ---..
    ..#-- ----- -=#.. ..-..
    ..... -.... ..... ..-..
    
    ..... -.... ..... ..-..
    ..... =#=-- -#... ..-..
    ..... ---.. ..... ..-..
    ..... .X... ..... ..-..
    ..... ..... ..... ..#..
""".strip()


class MapPos(NamedTuple):
    x: int
    y: int

    def __eq__(self, other):
        return (other.x == self.x
            and other.y == self.y)


class MapTileType(Enum):
    Empty = 'assets/sand.png'
    Village = 'assets/village.png'
    Castle = 'assets/castle.png'
    Path = 'assets/path.png'


class MapTile(EventDispatcher):
    stationed_units = ListProperty() # type: List[Unit]
    def __init__(self, pos: MapPos, type: MapTileType) -> None:
        self.pos = pos
        self.type = type
        self.image = type.value
        self.events = []
        super().__init__()

    def connects_with(self, other: 'MapTile') -> bool:
        return other.type != MapTileType.Empty

    def __str__(self):
        return '{} @ {}'.format(self.type, self.pos)

    def get_notification_name(self):
        return '({}, {})'.format(self.pos.x, self.pos.y)

    @property
    def soldier_count(self) -> int:
        from .units import Soldier
        return len([unit for unit in self.stationed_units if isinstance(unit, Soldier)])

    @property
    def spy_count(self) -> int:
        from .units import Spy
        return len([unit for unit in self.stationed_units if isinstance(unit, Spy)])


class Village(MapTile, HasPopupPanel):
    VILLAGE_NAMES = [
        'Abalathia',
        'Byron',
        'Chessington',
        'Daemon',
        'Edgeworth',
        'Finn',
        'Gradia',
        'Halone',
        'Ingress',
        'Juniper',
        'Kork',
        'Lysaburg',
        'Mafra',
        'Newton',
        'Ogham',
        'Plone',
        'Quoth',
        'Ryme',
        'Stain',
        'Tangent',
        'U\'Ghamaro',
        'Viola',
        'Westford',
        'Xavier',
        'Youngsville',
        'Zalera'
    ]
    target_soldier_count = NumericProperty()
    popup_actions = [
        ('Tax', 'tax'),
    ]
    popup_metrics = [
        ('Unrest: {}', 'unrest'),
        ('Pop: {}', 'villagers'),
        ('Troops: {}', 'soldier_count'),
        ('Spies: {}', 'spy_count')
    ]
    popup_adjustables = [
        ('Wanted Troops: {}', 'target_soldier_count'),
        ('Wanted Spies: {}', 'target_spy_count')
    ]
    
    def __init__(self, pos: MapPos, game: 'Vizier') -> None:
        super().__init__(pos, MapTileType.Village)
        self.name = random.choice(Village.VILLAGE_NAMES)
        Village.VILLAGE_NAMES.remove(self.name)
        self.popup_title = self.name
        self.register_event_type('on_tax')
        self.register_event_type('on_target_soldier_count')
        self.register_event_type('on_target_spy_count')
        self.bind(stationed_units=lambda _, units: self.update_labels())
        self.timers = {
            'spawn_check': 0.0,
            'unrest_drop': 0.0
        }
        self.time_since_last_spawn_check = 0.0
        self.time_since_last_unrest_drop = 0.0
        self.villagers = randint(20, 100)
        self.tax_amount = int(self.villagers * 0.3)
        self.popup_actions = [
            ('Tax ({}g)'.format(self.tax_amount), 'tax')
        ]
        self.target_soldier_count = 0
        self.pending_soldier_count = 0
        self.target_spy_count = 0
        self.pending_spy_count = 0
        self.unrest = 10
        self.game = game

    def tick(self, dt: float) -> None:
        from .units import Rebel
        for timer in self.timers:
            self.timers[timer] += dt
        while self.timers['spawn_check'] > 30:
            self.timers['spawn_check'] -= 30
            chance = randint(0, 200)
            if chance < min(100, self.unrest + self.game.global_unrest):
                self.game.spawn_unit(Rebel(self.game, self.game.map, self.pos))
                self.game.fire_notification('Rebel\'s have risen up at {} ({}, {})'.format(self.name, self.pos.x, self.pos.y))
        while self.timers['unrest_drop'] > 60:
            self.timers['unrest_drop'] -= 60
            self.unrest = max(self.unrest - 3, 0)

    def get_notification_name(self):
        return '{} ({}, {})'.format(self.name, self.pos.x, self.pos.y)

    def on_tax(self) -> None: 
        self.game.gold += self.tax_amount
        self.game.global_unrest += 2
        self.unrest += 20

    def on_target_soldier_count(self, old_value, new_value) -> None: 
        pass

    @property
    def incoming_soldier_count(self) -> int:
        return self.soldier_count + self.pending_soldier_count

    @property
    def troop_neediness(self) -> int:
        return self.target_soldier_count - self.incoming_soldier_count

    def on_target_spy_count(self, old_value, new_value) -> None: 
        pass

    @property
    def incoming_spy_count(self) -> int:
        return self.spy_count + self.pending_spy_count

    @property
    def spy_neediness(self) -> int:
        return self.target_spy_count - self.incoming_spy_count


class Castle(MapTile, HasPopupPanel):
    popup_title = 'Castle'
    popup_actions = [
        ('Hire Troops (20g)', 'troops'),
        ('Hire Spy (40g)', 'spy')
    ]
    popup_metrics = [
        ('Troops: {}', 'soldier_count')
    ]
    popup_adjustables: List[Tuple[str, str]] = []

    def __init__(self, pos: MapPos, game: 'Vizier') -> None:
        super().__init__(pos, MapTileType.Castle)
        self.register_event_type('on_troops')
        self.register_event_type('on_spy')
        self.game = game

    def get_notification_name(self):
        return 'the castle'

    def on_troops(self) -> None:
        if self.game.gold < 20:
            self.game.fire_notification('Not enough gold to hire a soldier')
            return
        from .units import Soldier
        soldier = Soldier(self.game, self.game.map, self.pos)
        self.game.spawn_unit(soldier)
        self.stationed_units.append(soldier)
        self.game.gold -= 20

    def on_spy(self) -> None:
        if self.game.gold < 20:
            self.game.fire_notification('Not enough gold to hire a spy')
            return
        from .units import Spy
        spy = Spy(self.game, self.game.map, self.pos)
        self.game.spawn_unit(spy)
        self.stationed_units.append(spy)
        self.game.gold -= 40

class Path(MapTile):
    def __init__(self, pos: MapPos) -> None:
        super().__init__(pos, MapTileType.Path)

    def connects_with(self, other: MapTile) -> bool:
        return other.type != MapTileType.Empty

    def fix_image(self, map: 'Map') -> None:
        x = self.pos.x
        y = self.pos.y
        neighbours = [
            (map.get_tile(x, y - 1), 'n'),
            (map.get_tile(x + 1, y), 'e'),
            (map.get_tile(x, y + 1), 's'),
            (map.get_tile(x - 1, y), 'w')
        ]
        directions = []
        for neighbour, direction in neighbours:
            if neighbour and self.connects_with(neighbour):
                directions.append(direction)
        self.image = 'assets/path-{}.png'.format(
            ''.join(directions)
        )

    
class Highway(Path):
    def connects_with(self, other: MapTile):
        return isinstance(other, Path)


class Map:
    def __init__(self, game: 'Vizier', map_layout: str = None) -> None:
        map_layout = DEFAULT_MAP_LAYOUT if map_layout is None else map_layout
        self.game = game
        self.villages = [] # type: List[Village]
        self.castle = None # type: Castle
        self.width = 20
        self.height = 20
        self.tiles = [] # type: List[MapTile]
        self.parse_map_layout(map_layout)

    @overload
    def get_tile(self, pos: MapPos) -> MapTile: ...
    
    @overload
    def get_tile(self, x: int, y: int) -> MapTile: ...

    def get_tile(self, x, y=None):
        if isinstance(x, MapPos):
            y = x.y
            x = x.x
        if 0 > x >= self.width or 0 > y >= self.height:
            return None
        return self.tiles[(y * self.width) + x]

    def parse_map_layout(self, map_layout: str = None) -> None:
        raw_layout = map_layout.replace(' ', '').split('\n')
        raw_layout = [line for line in raw_layout if line != '']
        self.width = len(raw_layout[0])
        self.height = len(raw_layout)
        paths = []
        for y, line in enumerate(raw_layout):
            for x, cell in enumerate(line):
                tile: MapTile
                pos = MapPos(x=x, y=y)
                if cell == '#':
                    tile = Village(pos, self.game)
                    self.villages.append(tile)
                elif cell == 'X':
                    tile = Castle(pos, self.game)
                    self.castle = tile
                elif cell == '-':
                    tile = Path(pos)
                    paths.append(tile)
                elif cell == '=':
                    tile = Highway(pos)
                    paths.append(tile)
                else:
                    tile = MapTile(pos, MapTileType.Empty)
                self.tiles.append(tile)

        for path in paths:
            path.fix_image(self)