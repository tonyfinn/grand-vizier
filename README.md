![upload-ss.png](assets/upload-ss.png)

# Grand Vizier

You are the Grand Vizier, newly appointed ruler of this desert kingdom!

There's just one problem, the rightful heir has decided to become a hero. You need to stop this and catch him. But try not upset the people too much. Remember: The tighter you squeeze, the more they will slip through your fingers. The more troops you raise and more tax you raise to support securing your power, the worse your rebel problem will become.

## Gameplay instructions

1. Send spies to villages to track down rebel/hero activity. To do so, click the village on the map and adjust the target spies.
2. After a short while you will get a status report. If you find the hero, send soldiers to kill him. It will take 5 soldiers to subdue him.
3. To recruit more soldiers/spies, go to your castle and click the button. Soldiers cost 20 gold, and spies cost 40 gold.
4. If you run out of gold, you can tax the people to get more gold. Be warned, that upsets the taxed village (a lot!) and also raises the global unrest slightly.

## Setup instructions

### Windows

1. Download the windows zip file.
2. Unzip
3. Run vizier.exe

### Linux

1. Install Python 3.6 (see your distro package manager. May require PPAs)
2. [Install Kivy](https://kivy.org/docs/installation/installation-linux.html)
3. Download the repository dump.
4. Run play-linux.sh

### Mac

1. Try the linux instructions, but not officially supported